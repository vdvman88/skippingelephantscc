﻿using UnityEngine;
using System.Collections;
using Assets.Scripts;
using UnityEditor;
using UnityEngine.UI;

[CustomPropertyDrawer(typeof(TwoDimArray))]
public class TwoDimArrayDrawer : PropertyDrawer
{
    private readonly float _size = EditorGUIUtility.singleLineHeight;
    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
    {
        EditorGUI.PrefixLabel(position, label);
        var manager = property.serializedObject.targetObject as NodeManager;
        if (manager == null) return;

        Rect newPosition = position;
        newPosition.width = _size;
        newPosition.height = _size;
        SerializedProperty rows = property.FindPropertyRelative("Rows");
        if (rows.arraySize != manager.Height)
        {
            rows.arraySize = manager.Height;
        }
        for (int y = manager.Height - 1; y >= 0; --y)
        {
            newPosition.x = position.x;
            newPosition.y += _size;
            SerializedProperty row = rows.GetArrayElementAtIndex(y).FindPropertyRelative("Row");
            if (row.arraySize != manager.Width)
            {
                row.arraySize = manager.Width;
            }
            for (var x = 0; x < manager.Width; x++)
            {
                EditorGUI.PropertyField(newPosition, row.GetArrayElementAtIndex(x), GUIContent.none);
                newPosition.x += newPosition.width;
            }
        }
    }

    public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
    {
        var manager = property.serializedObject.targetObject as NodeManager;
        if (manager == null) return 0;

        return _size * (manager.Height + 1);
    }
}
