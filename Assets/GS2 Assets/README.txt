This is a small subset of the Gamestrap UI package (https://www.assetstore.unity3d.com/en/#!/content/28599)

It is repackaged here for teaching and learning purposes. 

Get more features from this package and support the original authors through the link above.