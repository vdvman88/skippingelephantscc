﻿using UnityEngine;
using System.Collections;
using System;
using Random = UnityEngine.Random;

public class Node : MonoBehaviour
{
    public enum NodeColor
    {
        Red,
        Green,
        Blue,
        Wild
    }
    public static readonly Color[] NodeColors = {
        UnityEngine.Color.red, UnityEngine.Color.green, UnityEngine.Color.blue};

    [HideInInspector]
    public NodeColor Color { get; protected set; }
    public int XIndex { get; internal set; }
    public int YIndex { get; internal set; }
    private SpriteRenderer _spriteRenderer;

    public void Initialise(int xIndex, int yIndex, bool randomColor)
    {
        if(randomColor)
        {
            RandomColor();
        }
        XIndex = xIndex;
        YIndex = yIndex;
        _spriteRenderer = GetComponent<SpriteRenderer>();
        if (Color != NodeColor.Wild)
        {
            _spriteRenderer.color = NodeColors[(int)Color];
        }
    }

    public virtual void Initialise(int xIndex, int yIndex)
    {
        Initialise(xIndex, yIndex, true);
    }

    public void RandomColor()
    {
        int color = Random.Range(0, NodeColors.Length);
        Color = (NodeColor) color;
    }

    public virtual int OnRemoved(NodeManager manager, int scoreChange)
    {
        return scoreChange;
    }

    public virtual bool Matches(Node other)
    {
        return other.Color == Color || other.Color == NodeColor.Wild;
    }
}
