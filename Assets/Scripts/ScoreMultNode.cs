﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets.Scripts
{
    class ScoreMultNode : SpecialNode
    {

        public override int OnRemoved(NodeManager nodeManager, int scoreChange)
        {
            nodeManager.ScoreMult += nodeManager.ScoreMultIncrease;
            return scoreChange;
        }
    }

}
