﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets.Scripts
{
    class AntidoteNode : SpecialNode
    {

        public override int OnRemoved(NodeManager nodeManager, int scoreChange)
        {
            nodeManager.IsProcessingAntidote = true;

            //replace all negative nodes on board with generic wild-type
            for (var x = 0; x < nodeManager.Width; x++)
            {
                for (var y = 0; y < nodeManager.Height; y++)
                {
                    if(nodeManager.NodeAt(x,y) != null)
                    {
                        if((nodeManager.NodeAt(x,y).GetType() == typeof(Poison) || nodeManager.NodeAt(x,y).GetType() == typeof(Negate)))
                        {
                            nodeManager.ReplaceNode(x, y, nodeManager.WildNodePrefab);
                        }
                    }
                }
            }
            return scoreChange;
        }
    }

}
