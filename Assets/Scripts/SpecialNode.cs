﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets.Scripts
{
    class SpecialNode : Node
    {
        public override void Initialise(int xIndex, int yIndex)
        {
            Color = NodeColor.Wild;
            base.Initialise(xIndex, yIndex, false);
        }

        public override bool Matches(Node other)
        {
            return false;   //can be matched to, but will not propagate matching
        }
    }
}
