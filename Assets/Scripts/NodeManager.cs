﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Assets.Scripts;
using UnityEngine;
using UnityEngine.UI;
using Random = UnityEngine.Random;

public class NodeManager : MonoBehaviour
{

    public GameObject NormalNodePrefab;
    public GameObject WildNodePrefab;
    public GameObject[] PowerUpPrefabs;
    public GameObject[] PowerDownPrefabs;
    public GameObject GridPrefab;
    public int Width;
    public int Height;
    [SerializeField] private int _connectedCount = 2;
    public Tweener Tweener;
    public Text ScoreText;
    public Text ScoreMultText;
    public int MaxReshuffles;
    public Image LoadingIcon;
    public GameObject GameOverUI;
    public GameObject LevelCompleteUI;
    public TwoDimArray ActiveGridPositions;
    public float ScoreMultIncrease = 0.5f;
    public float ScoreMultDecay = 0.1f;
    public int ScoreGoal;
    public int NormalNodeWeight;
    public int PowerUpWeight;
    public int PowerDownWeight;

    private float _normalNodeProb;
    private float _powerUpProb;
    private float _powerDownProb;
    [HideInInspector] public bool IsProcessingAntidote;
    [HideInInspector] public bool IsProcessingNegate;
    [HideInInspector] public int NumPoisonBeingProcessed;
    [HideInInspector] public double ScoreMult;

    private Node[][] _grid;
    private Stack<Node> _nodeCache;
    private bool[][] _deletion;
    private int _score;
    private float _xTrans;
    private float _yTrans;
    private string _scorePrefix;
    private string _scoreMultPrefix;
    private int _reshuffleCount;
    private Coroutine _reshuffle = null;
    private bool _levelComplete = false;
    
    public void Start ()
    {
        CameraController.CorrectOrthographicSize();
        if (PowerUpPrefabs.Length == 0)
        {
            PowerUpWeight = 0;
        }
        if (PowerDownPrefabs.Length == 0)
        {
            PowerDownWeight = 0;
        }
        int totalWeight = NormalNodeWeight + PowerUpWeight + PowerDownWeight;
        if (totalWeight == 0)
        {
            totalWeight = 1;
        }
        _normalNodeProb = NormalNodeWeight/(float) totalWeight;
        _powerDownProb = PowerDownWeight/(float) totalWeight;
        _powerUpProb = PowerUpWeight/(float) totalWeight;
        _reshuffleCount = 0;
        _score = 0;
        _scorePrefix = ScoreText.text;
        ScoreMult = 1.0;
        _scoreMultPrefix = ScoreMultText.text;
        UpdateScore();
	    _grid = new Node[Width][];
        _nodeCache = new Stack<Node>(Width * Height);
        _deletion = new bool[Width][];
        Tweener.Initialize(Width * Height);
	    _xTrans = (Width - 1)/2.0f;
	    _yTrans = (Height - 1)/2.0f;
        for (var i = 0; i < Width; i++)
	    {
            _grid[i] = new Node[Height];
            _deletion[i] = new bool[Height];
	        for (var j = 0; j < Height; j++)
	        {
                if (!ActiveGridPositions[i, j]) continue;

                _grid[i][j] = NewNode(i, j);
                
#pragma warning disable 0168
	            // ReSharper disable once UnusedVariable
                var gridNode = (GameObject) Instantiate(GridPrefab, GridToWorld(i, j), Quaternion.identity); // Initialise grid, don't need a reference
#pragma warning restore 0168
            }
	    }
        if (_reshuffle == null)
        {
            _reshuffle = StartCoroutine(CheckForMoves(true));
        }
    }

    public void Update () {
        if (_score >= ScoreGoal)
        {
            LevelCompleteUI.SetActive(true);
            _levelComplete = true;
        }
        if (!_levelComplete && !Tweener.IsTweening)
        {
            if(_reshuffle == null)
            {
                _reshuffle = StartCoroutine(CheckForMoves(false));
            }

            if (Input.GetMouseButtonDown(0))
            {
                int layer = LayerMask.GetMask("Nodes");
                RaycastHit2D hit = Physics2D.GetRayIntersection(Camera.main.ScreenPointToRay(Input.mousePosition), float.PositiveInfinity, layer);
                if (hit.collider == null) return;

                var node = hit.collider.GetComponent<Node>();
                int count = GetConnected(node, true);
                if (count > _connectedCount)
                {
                    var scoreChange = 0;
                    var removedNodes = new List<Node>();
                    for (var x = 0; x < Width; x++)
                    {
                        for (var y = 0; y < Height; y++)
                        {
                            if (!_deletion[x][y]) continue;

                            removedNodes.Add(_grid[x][y]);
                            RemoveNode(x, y);
                            scoreChange++;
                        }
                    }
                    scoreChange = removedNodes.Aggregate(scoreChange, (current, removedNode) => removedNode.OnRemoved(this, current));
                    scoreChange *= 10;
                    if(!IsProcessingAntidote)
                    {
                        if(NumPoisonBeingProcessed > 0)
                        {
                            scoreChange *= -NumPoisonBeingProcessed;
                        }
                        else if(IsProcessingNegate)
                        {
                            scoreChange = 0;
                        }
                    }
                    if(scoreChange > 0)
                    {
                        scoreChange = (int)(scoreChange * ScoreMult);
                    }
                    IsProcessingAntidote = false;
                    IsProcessingNegate = false;
                    NumPoisonBeingProcessed = 0;
                    _score += scoreChange;
                    Fall();
                    UpdateScore();
                }
            }
        }
    }

    public void NextLevel()
    {
        FindObjectOfType<LevelManager>().NextLevel();
    }

    private IEnumerator CheckForMoves(bool first)
    {
        if (HasMoves())
        {
            _reshuffle = null;
            yield break;
        }
        if (first || _reshuffleCount < MaxReshuffles || MaxReshuffles < 0)
        {
            ++_reshuffleCount;
            for (var i = 1; !HasMoves(); ++i)
            {
                ResetBoard();
                if (i%100 == 0)
                {
                    i = 1;
                    LoadingIcon.gameObject.SetActive(true);
                    yield return null;
                }
            }
            for (var x = 0; x < Width; x++)
            {
                for (var y = 0; y < Height; y++)
                {
                    if(!ActiveGridPositions[x, y]) continue;

                    _grid[x][y].gameObject.SetActive(true);
                    Tweener.AddTween(_grid[x][y].transform, GridToWorld(x, y));
                }
            }
            LoadingIcon.gameObject.SetActive(false);
        }
        else
        {
            GameOverUI.gameObject.SetActive(true);
            yield return new WaitForSeconds(1);
            FindObjectOfType<LevelManager>().GoToMenu();
        }
        _reshuffle = null;
    } 

    private bool HasMoves()
    {
        ResetDeletion();
        for (var x = 0; x < Width; x++)
        {
            for (var y = 0; y < Height; y++)
            {
                if (_deletion[x][y] || !ActiveGridPositions[x, y]) continue;
                int count = GetConnected(_grid[x][y], false);
                if (count > _connectedCount)
                {
                    return true;
                }
            }
        }
        return false;
    }

    private void ResetBoard()
    {
        for (var x = 0; x < Width; x++)
        {
            for (var y = 0; y < Height; y++)
            {
                if(!ActiveGridPositions[x, y]) continue;
                RemoveNode(x, y);
                _grid[x][y] = NewNode(x, y);
                _grid[x][y].transform.position = GridToWorld(x, Height + 2);
                _grid[x][y].gameObject.SetActive(false);
            }
        }
    }

    private void UpdateScore()
    {
        //Decay ScoreMult towards 1.0
        if(ScoreMult > 1.0 + ScoreMultDecay)
        {
            ScoreMult -= ScoreMultDecay;
        }
        else if(ScoreMult < 1.0 - ScoreMultDecay)
        {
            ScoreMult += ScoreMultDecay;
        }
        else
        {
            ScoreMult = 1.0;
        }
        ScoreMultText.text = string.Format("{0}{1:F}", _scoreMultPrefix, ScoreMult);
        ScoreText.text = _scorePrefix + _score;
    }

    private void Fall()
    {
        for (var x = 0; x < Width; x++)
        {
            for (var y = 0; y < Height; y++)
            {
                if (_grid[x][y] != null || !ActiveGridPositions[x, y]) continue;

                var success = false;

                for (int i = y + 1; i < Height; i++)
                {
                    if (_grid[x][i] == null || !ActiveGridPositions[x, y]) continue;

                    success = true;
                    _grid[x][y] = _grid[x][i];
                    _grid[x][i] = null;
                    _grid[x][y].YIndex = y;
                    Tweener.AddTween(_grid[x][y].transform, GridToWorld(x, y));
                    break;
                }
                if (!success)
                {
                    _grid[x][y] = NewNode(x, y);
                    _grid[x][y].transform.position = GridToWorld(x, Height + 2);
                    Tweener.AddTween(_grid[x][y].transform, GridToWorld(x, y));
                }
            }
        }
    }

    private void ResetDeletion()
    {
        for (var i = 0; i < Width; i++)
        {
            for (var j = 0; j < Height; j++)
            {
                _deletion[i][j] = false;
            }
        }
    }
    
    /**
     *  Basically just a flood fill algorithm
     */
    private int GetConnected(Node node, bool reset)
    {
        if (reset)
        {
            ResetDeletion();
        }
        var count = 0;
        var queue = new Queue<Node>(Width * Height);
        queue.Enqueue(node);
        while (queue.Count > 0)
        {
            Node n = queue.Dequeue();
            int x = n.XIndex;
            int y = n.YIndex;
            ++count;

            if (x > 0 && ActiveGridPositions[x - 1, y] && n.Matches(_grid[x - 1][y]) && !_deletion[x - 1][y])
            {
                queue.Enqueue(_grid[x - 1][y]);
                _deletion[x - 1][y] = true;
            }
            if (x < Width - 1 && ActiveGridPositions[x + 1, y] && n.Matches(_grid[x + 1][y]) && !_deletion[x + 1][y])
            {
                queue.Enqueue(_grid[x + 1][y]);
                _deletion[x + 1][y] = true;
            }
            if (y > 0 && ActiveGridPositions[x, y - 1] && n.Matches(_grid[x][y - 1]) && !_deletion[x][y - 1])
            {
                queue.Enqueue(_grid[x][y - 1]);
                _deletion[x][y - 1] = true;
            }
            if (y < Height - 1 && ActiveGridPositions[x, y + 1] && n.Matches(_grid[x][y + 1]) && !_deletion[x][y + 1])
            {
                queue.Enqueue(_grid[x][y + 1]);
                _deletion[x][y + 1] = true;
            }
        }
        return count;
    }

    private Node NewNode(int x, int y)
    {
        Node node;
        float rand = Random.value;
        rand -= _normalNodeProb;
        if(rand <= 0)
        {
            if (_nodeCache.Count > 0)
            {
                node = _nodeCache.Pop();
                node.gameObject.SetActive(true);
            }
            else
            {
                GameObject go = Instantiate(NormalNodePrefab);
                node = go.GetComponent<Node>();
            }
        }
        else
        {
            rand -= _powerDownProb;
            if (rand <= 0)
            {
                int nodeID = Random.Range(0, PowerDownPrefabs.Length);
                GameObject go = Instantiate(PowerDownPrefabs[nodeID]);
                node = go.GetComponent<Node>();
            }
            else
            {
                int nodeID = Random.Range(0, PowerUpPrefabs.Length);
                GameObject go = Instantiate(PowerUpPrefabs[nodeID]);
                node = go.GetComponent<Node>();
            }
        }

        node.Initialise(x, y);
        node.transform.position = GridToWorld(x, y);
        return node;
    }

    private Node NewNode(int x, int y, GameObject prefabType)
    {

        GameObject go = Instantiate(prefabType);
        Node node = go.GetComponent<Node>();

        node.Initialise(x, y);
        node.transform.position = GridToWorld(x, y);
        return node;
    }

    public Node NodeAt(int x, int y)
    {
        return _grid[x][y];
    }

    public void ReplaceNode(int x, int y, GameObject replacementPrefabType)
    {
        //should probably exception check replacementPrefabType input

        RemoveNode(x, y);
        _grid[x][y] = NewNode(x, y, replacementPrefabType);
    }

    private void RemoveNode(int x, int y)
    {
        Node node = _grid[x][y];
        _grid[x][y] = null;
        node.gameObject.SetActive(false);
        if (node.Color != Node.NodeColor.Wild)
        {
            _nodeCache.Push(node);
        }
    }

    private Vector2 GridToWorld(int x, int y)
    {
        return new Vector2(x - _xTrans, y - _yTrans);
    }
}
