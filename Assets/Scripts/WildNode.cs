﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets.Scripts
{
    class WildNode : SpecialNode
    {
        public override int OnRemoved(NodeManager nodeManager, int scoreChange)
        {
            return scoreChange;
        }

        public override bool Matches(Node other)
        {
            return other.Color == NodeColor.Wild;   //return other.GetType() == this.GetType();
        }
    }
}
