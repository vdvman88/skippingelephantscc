﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets.Scripts
{
    class Negate : SpecialNode
    {
        public override int OnRemoved(NodeManager nodeManager, int scoreChange)
        {
            nodeManager.IsProcessingNegate = true;
            return scoreChange;
        }
    }
}
