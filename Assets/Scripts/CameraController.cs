﻿using UnityEngine;

public class CameraController : MonoBehaviour
{
    private const float TargetWidth = 9;
    private const float TargetHeight = 16;

    private static float TargetAspectRatio
    {
        get { return TargetWidth/TargetHeight; }
    }

    private static float CurrentAspectRatio
    {
        get { return (float) Screen.width/Screen.height; }
    }

    private static bool SmallerWidthToHeight
    {
        get { return CurrentAspectRatio < TargetAspectRatio; }
    }

    public static void CorrectOrthographicSize()
    {
        if (!SmallerWidthToHeight) return;

        float orthographicWidth = Camera.main.orthographicSize * TargetAspectRatio;
        Camera.main.orthographicSize = orthographicWidth / CurrentAspectRatio;
    }
}
