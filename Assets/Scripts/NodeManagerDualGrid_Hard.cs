﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class NodeManagerDualGrid_Hard : MonoBehaviour
{
    public GameObject NodePrefab;
    public GameObject StarPrefab;
    public GameObject GridPrefab;
    public Tweener Tweener;
    public Text ScoreText;
    public Text ComboText;
    public GameObject GameOverUI;
    public GameObject LevelCompleteUI;
    private GameObject gridNode;
    private int _connectedCount = 2;
    private int Grid;
    private int Width;
    private int Height;
    private int MaxHeight;
    private int _score;
    private int _combo = 1;
    private Node[][] _star;
    public int ScoreGoal;
    private Node[][] _grid_T;
    private Node[][] _grid_B;
    private GameObject Star_LL;
    private GameObject Star_L;
    private GameObject Star_C;
    private GameObject Star_R;
    private GameObject Star_RR;
    private Stack<Node> _nodeCache;
    private GameObject[][] GridNode_T;
    private GameObject[][] GridNode_B;
    private bool[][] _deletion_T;
    private bool[][] _deletion_B;
    private int _column;
    private float _currTime;
    private float _fadeTime_LL;
    private float _fadeTime_L;
    private float _fadeTime_C;
    private float _fadeTime_R;
    private float _fadeTime_RR;
    private float _scoreTime_LL;
    private float _scoreTime_L;
    private float _scoreTime_C;
    private float _scoreTime_R;
    private float _scoreTime_RR;
    private float target_LL = 0.0f;
    private float target_L = 0.0f;
    private float target_C = 0.0f;
    private float target_R = 0.0f;
    private float target_RR = 0.0f;
    private float _xTrans;
    private float _yTrans_T;
    private float _yTrans_B;
    private float _duration = 0.375f;
    private string _scorePrefix;
    private string _comboPrefix;
    private bool _levelComplete = false;
    private Coroutine _gameOver = null;

    public void Start()
    {
        Height = UnityEngine.Random.Range(3, 6);

        CameraController.CorrectOrthographicSize();
        Width = 5;
        _score = 0;
        _scorePrefix = ScoreText.text;
        _comboPrefix = ComboText.text;
        UpdateScore();
        UpdateCombo();
        GridNode_T = new GameObject[Width][];
        GridNode_B = new GameObject[Width][];
        _star = new Node[Width][];
        _grid_T = new Node[Width][];
        _grid_B = new Node[Width][];
        _nodeCache = new Stack<Node>(Width * Height);
        _deletion_T = new bool[Width][];
        _deletion_B = new bool[Width][];
        Tweener.Initialize(Width * Height);
        MaxHeight = 8;
        _xTrans = (Width - 1) / 2.0f;
        _yTrans_T = 4;
        _yTrans_B = 3 - Height;
        for (var i = 0; i < Width; i++)
        {
            GridNode_T[i] = new GameObject[Height];
            _star[i] = new Node[1];
            _grid_T[i] = new Node[Height];
            _deletion_T[i] = new bool[Height];
            for (var j = 0; j < Height; j++)
            {
                _grid_T[i][j] = NewNode(i, j, 1);

#pragma warning disable 0168
                // ReSharper disable once UnusedVariable
                GridNode_T[i][j] = Instantiate(GridPrefab, GridToWorld(i, j, 1), Quaternion.identity) as GameObject; // Initialise grid, don't need a reference
#pragma warning restore 0168
            }
        }
        for (var i = 0; i < Width; i++)
        {
            for (var j = 0; i < Width; i++)
            {
                _star[i][j] = NewNode(i, j, 1);
                gridNode = (GameObject)Instantiate(GridPrefab, GridToWorld(i, j, 3), Quaternion.identity); // Initialise grid, don't need a reference
                gridNode.GetComponent<SpriteRenderer>().color = new Color(1.0f, 0.65f, 0.0f, 1.0f);
            }
        }
        Star_LL = (GameObject)Instantiate(StarPrefab, GridToWorld(0, 0, 3), Quaternion.identity);
        Star_L = (GameObject)Instantiate(StarPrefab, GridToWorld(1, 0, 3), Quaternion.identity);
        Star_C = (GameObject)Instantiate(StarPrefab, GridToWorld(2, 0, 3), Quaternion.identity);
        Star_R = (GameObject)Instantiate(StarPrefab, GridToWorld(3, 0, 3), Quaternion.identity);
        Star_RR = (GameObject)Instantiate(StarPrefab, GridToWorld(4, 0, 3), Quaternion.identity);
        for (var i = 0; i < Width; i++)
        {
            GridNode_B[i] = new GameObject[MaxHeight - Height];
            _grid_B[i] = new Node[MaxHeight - Height];
            _deletion_B[i] = new bool[MaxHeight - Height];
            for (var j = 0; j < (MaxHeight - Height); j++)
            {
                _grid_B[i][j] = NewNode(i, j, 2);

#pragma warning disable 0168
                // ReSharper disable once UnusedVariable
                GridNode_B[i][j] = Instantiate(GridPrefab, GridToWorld(i, j, 2), Quaternion.identity) as GameObject; // Initialise grid, don't need a reference
#pragma warning restore 0168
            }
        }
    }


    public void Update()
    {
        if (_score >= ScoreGoal)
        {
            LevelCompleteUI.SetActive(true);
            _levelComplete = true;
        }
        if (_gameOver == null && !HasMoves(1) && !HasMoves(2))
        {
            _gameOver = StartCoroutine(GameOver());
        }
        if (_currTime > _scoreTime_LL)
            {
                for (var j = 0; j < Height; j++)
                {
                    GridNode_T[0][j].GetComponent<SpriteRenderer>().color = new Vector4(1.0f, 1.0f, 1.0f, 1.0f);

                }
                for (var j = 0; j < (MaxHeight - Height); j++)
                {
                    GridNode_B[0][j].GetComponent<SpriteRenderer>().color = new Vector4(1.0f, 1.0f, 1.0f, 1.0f);
                }
            }
            if (_currTime > _scoreTime_L)
            {
                for (var j = 0; j < Height; j++)
                {
                    GridNode_T[1][j].GetComponent<SpriteRenderer>().color = new Vector4(1.0f, 1.0f, 1.0f, 1.0f);

                }
                for (var j = 0; j < (MaxHeight - Height); j++)
                {
                    GridNode_B[1][j].GetComponent<SpriteRenderer>().color = new Vector4(1.0f, 1.0f, 1.0f, 1.0f);
                }
            }
            if (_currTime > _scoreTime_C)
            {
                for (var j = 0; j < Height; j++)
                {
                    GridNode_T[2][j].GetComponent<SpriteRenderer>().color = new Vector4(1.0f, 1.0f, 1.0f, 1.0f);

                }
                for (var j = 0; j < (MaxHeight - Height); j++)
                {
                    GridNode_B[2][j].GetComponent<SpriteRenderer>().color = new Vector4(1.0f, 1.0f, 1.0f, 1.0f);
                }
            }
            if (_currTime > _scoreTime_R)
            {
                for (var j = 0; j < Height; j++)
                {
                    GridNode_T[3][j].GetComponent<SpriteRenderer>().color = new Vector4(1.0f, 1.0f, 1.0f, 1.0f);

                }
                for (var j = 0; j < (MaxHeight - Height); j++)
                {
                    GridNode_B[3][j].GetComponent<SpriteRenderer>().color = new Vector4(1.0f, 1.0f, 1.0f, 1.0f);
                }
            }
            if (_currTime > _scoreTime_RR)
            {
                for (var j = 0; j < Height; j++)
                {
                    GridNode_T[4][j].GetComponent<SpriteRenderer>().color = new Vector4(1.0f, 1.0f, 1.0f, 1.0f);

                }
                for (var j = 0; j < (MaxHeight - Height); j++)
                {
                    GridNode_B[4][j].GetComponent<SpriteRenderer>().color = new Vector4(1.0f, 1.0f, 1.0f, 1.0f);
                }
            }
        
        _currTime = Time.time;
        if (_currTime > _scoreTime_LL && _currTime > _scoreTime_LL && _currTime > _scoreTime_L && _currTime > _scoreTime_C && _currTime > _scoreTime_R && _currTime > _scoreTime_RR)
        {
            _combo = 1;
        }
        UpdateCombo();
        // Decrease alpha transparency value of column icons
        Star_LL.GetComponent<SpriteRenderer>().color = new Color(1.0f, 1.0f, 0.19f, Mathf.Lerp(Star_LL.GetComponent<SpriteRenderer>().color.a, target_LL, Time.deltaTime / _duration));
        Star_L.GetComponent<SpriteRenderer>().color = new Color(1.0f, 1.0f, 0.19f, Mathf.Lerp(Star_L.GetComponent<SpriteRenderer>().color.a, target_L, Time.deltaTime / _duration));
        Star_C.GetComponent<SpriteRenderer>().color = new Color(1.0f, 1.0f, 0.19f, Mathf.Lerp(Star_C.GetComponent<SpriteRenderer>().color.a, target_C, Time.deltaTime / _duration));
        Star_R.GetComponent<SpriteRenderer>().color = new Color(1.0f, 1.0f, 0.19f, Mathf.Lerp(Star_R.GetComponent<SpriteRenderer>().color.a, target_R, Time.deltaTime / _duration));
        Star_RR.GetComponent<SpriteRenderer>().color = new Color(1.0f, 1.0f, 0.19f, Mathf.Lerp(Star_RR.GetComponent<SpriteRenderer>().color.a, target_RR, Time.deltaTime / _duration));
        // Set alpha to 0 so it will eventually disappear as the combo time elapses
        if (_currTime > _fadeTime_LL)
        {
            target_LL = 0.0f;

        }
        if (_currTime > _fadeTime_L)
        {
            target_L = 0.0f;
        }
        if (_currTime > _fadeTime_C)
        {
            target_C = 0.0f;
        }
        if (_currTime > _fadeTime_R)
        {
            target_R = 0.0f;
        }
        if (_currTime > _fadeTime_RR)
        {
            target_RR = 0.0f;
        }
        if (!Tweener.IsTweening)
        {
            if (Input.GetMouseButtonDown(0))
            {
                int layer = LayerMask.GetMask("Nodes");
                RaycastHit2D hit = Physics2D.GetRayIntersection(Camera.main.ScreenPointToRay(Input.mousePosition), float.PositiveInfinity, layer);
                if (hit.collider == null) return;

                var node = hit.collider.GetComponent<Node>();
                CheckWidth(node);

                if (CheckGrid(node) == 1)
                {
                    int count = GetConnected(node, true, 1);
                    
                    if (count > _connectedCount)
                    {
                        for (var x = 0; x < Width; x++)
                        {
                            for (var y = 0; y < Height; y++)
                            {
                                if (!_deletion_T[x][y]) continue;

                                RemoveNode(x, y);

                                _score += _combo;
                            }
                        }
                        Fall(1);
                        UpdateScore();
                    }
                }
                else
                {
                    int count2 = GetConnected(node, true, 2);
                    
                    if (count2 > _connectedCount)
                    {

                        for (var x = 0; x < Width; x++)
                        {
                            for (var y = 0; y < ((MaxHeight - Height)); y++)
                            {
                                if (!_deletion_B[x][y]) continue;

                                RemoveNode2(x, y);
                                _score += _combo;
                            }
                        }
                        Fall(2);
                        UpdateScore();
                    }
                }
            }
        }
    }

    private bool HasMoves(int option)
    {
        ResetDeletion(option);
        switch (option)
        {

            case 1:

                for (var x = 0; x < Width; x++)
                {
                    for (var y = 0; y < Height; y++)
                    {
                        if (_deletion_T[x][y]) continue;
                        int count = GetConnected(_grid_T[x][y], false, 1);
                        if (count > _connectedCount)
                        {
                            return true;
                        }
                    }
                }
                break;
            case 2:
                for (var x = 0; x < Width; x++)
                {
                    for (var y = 0; y < MaxHeight - Height; y++)
                    {
                        if (_deletion_B[x][y]) continue;
                        int count = GetConnected(_grid_B[x][y], false, 2);
                        if (count > _connectedCount)
                        {
                            return true;
                        }
                    }
                }
                break;
        }
        return false;
    }

    private void UpdateScore()
    {
        ScoreText.text = _scorePrefix + _score;
    }
    private void UpdateCombo()
    {
        ComboText.text = _comboPrefix + _combo;
    }

    private void Fall(int option)
    {
        switch (option)
        {
            case 1:
                for (var x = 0; x < Width; x++)
                {
                    for (var y = 0; y < Height; y++)
                    {
                        if (_grid_T[x][y] != null) continue;

                        var success = false;

                        for (int i = y + 1; i < Height; i++)
                        {
                            if (_grid_T[x][i] == null) continue;

                            success = true;
                            _grid_T[x][y] = _grid_T[x][i];
                            _grid_T[x][i] = null;
                            _grid_T[x][y].YIndex = y;
                            Tweener.AddTween(_grid_T[x][y].transform, GridToWorld(x, y, 1));
                            break;
                        }
                        if (!success)
                        {
                            _grid_T[x][y] = NewNode(x, y, 1);
                            _grid_T[x][y].transform.position = GridToWorld(x, Height, 1);
                            Tweener.AddTween(_grid_T[x][y].transform, GridToWorld(x, y, 1));
                        }
                    }
                }
                break;
            case 2:
                for (var x = 0; x < Width; x++)
                {
                    for (var y = 0; y < (MaxHeight - Height); y++)
                    {
                        if (_grid_B[x][y] != null) continue;

                        var success = false;

                        for (int i = y + 1; i < (MaxHeight - Height); i++)
                        {
                            if (_grid_B[x][i] == null) continue;

                            success = true;
                            _grid_B[x][y] = _grid_B[x][i];
                            _grid_B[x][i] = null;
                            _grid_B[x][y].YIndex = y;
                            Tweener.AddTween(_grid_B[x][y].transform, GridToWorld(x, y, 2));
                            break;
                        }
                        if (!success)
                        {
                            _grid_B[x][y] = NewNode(x, y, 2);
                            _grid_B[x][y].transform.position = GridToWorld(x, MaxHeight - Height, 2);
                            Tweener.AddTween(_grid_B[x][y].transform, GridToWorld(x, y, 2));
                        }
                    }
                }
                break;
        }
    }


    private void ResetDeletion(int option)
    {
        switch (option)
        {
            case 1:
                for (var i = 0; i < Width; i++)
                {
                    for (var j = 0; j < Height; j++)
                    {
                        _deletion_T[i][j] = false;
                    }
                }
                break;
            case 2:
                for (var i = 0; i < Width; i++)
                {
                    for (var j = 0; j < MaxHeight - Height; j++)
                    {
                        _deletion_B[i][j] = false;
                    }
                }
                break;
        }

    }

    private int GetConnected(Node node, bool reset, int option)
    {


        var count = 0;
        switch (option)
        {
            case 1:

                if (reset)
                {
                    ResetDeletion(1);
                }
                var queue = new Queue<Node>(Width * Height);
                queue.Enqueue(node);
                while (queue.Count > 0)
                {
                    Node n = queue.Dequeue();
                    int x = n.XIndex;
                    int y = n.YIndex;
                    ++count;

                    if (x > 0 && _grid_T[x - 1][y].Color == node.Color && !_deletion_T[x - 1][y])
                    {
                        queue.Enqueue(_grid_T[x - 1][y]);
                        _deletion_T[x - 1][y] = true;
                    }
                    if (x < Width - 1 && _grid_T[x + 1][y].Color == node.Color && !_deletion_T[x + 1][y])
                    {
                        queue.Enqueue(_grid_T[x + 1][y]);
                        _deletion_T[x + 1][y] = true;
                    }
                    if (y > 0 && _grid_T[x][y - 1].Color == node.Color && !_deletion_T[x][y - 1])
                    {
                        queue.Enqueue(_grid_T[x][y - 1]);
                        _deletion_T[x][y - 1] = true;
                    }
                    if (y < Height - 1 && _grid_T[x][y + 1].Color == node.Color && !_deletion_T[x][y + 1])
                    {
                        queue.Enqueue(_grid_T[x][y + 1]);
                        _deletion_T[x][y + 1] = true;
                    }
                }
                break;
            case 2:

                if (reset)
                {
                    ResetDeletion(2);
                }
                queue = new Queue<Node>(Width * (MaxHeight - Height));
                queue.Enqueue(node);
                while (queue.Count > 0)
                {
                    Node n = queue.Dequeue();
                    int x = n.XIndex;
                    int y = n.YIndex;
                    ++count;

                    if (x > 0 && _grid_B[x - 1][y].Color == node.Color && !_deletion_B[x - 1][y])
                    {
                        queue.Enqueue(_grid_B[x - 1][y]);
                        _deletion_B[x - 1][y] = true;
                    }
                    if (x < Width - 1 && _grid_B[x + 1][y].Color == node.Color && !_deletion_B[x + 1][y])
                    {
                        queue.Enqueue(_grid_B[x + 1][y]);
                        _deletion_B[x + 1][y] = true;
                    }
                    if (y > 0 && _grid_B[x][y - 1].Color == node.Color && !_deletion_B[x][y - 1])
                    {
                        queue.Enqueue(_grid_B[x][y - 1]);
                        _deletion_B[x][y - 1] = true;
                    }
                    if (y < MaxHeight - Height - 1 && _grid_B[x][y + 1].Color == node.Color && !_deletion_B[x][y + 1])
                    {
                        queue.Enqueue(_grid_B[x][y + 1]);
                        _deletion_B[x][y + 1] = true;
                    }
                }
                break;
        }
        return count;
    }

    private Node NewNode(int x, int y, int option)
    {
        Node node;
        if (_nodeCache.Count > 0)
        {
            node = _nodeCache.Pop();
            node.gameObject.SetActive(true);
        }
        else
        {
            GameObject go = Instantiate(NodePrefab);
            node = go.GetComponent<Node>();
        }

        node.Initialise(x, y);
        switch (option)
        {
            case 1:
                node.transform.position = GridToWorld(x, y, 1);
                break;
            case 2:
                node.transform.position = GridToWorld(x, y, 2);
                break;
        }
        return node;
    }

    private int CheckGrid(Node node)
    {
        for (var i = 0; i < Width; i++)
        {

            for (var j = 0; j < Height; j++)
            {
                if (node == _grid_T[i][j])
                {
                    return (1);
                }
            }
        }

        for (var i = 0; i < Width; i++)
        {

            for (var j = 0; j < (MaxHeight - Height); j++)
            {
                if (node == _grid_B[i][j])
                {
                    return (2);
                }
            }
        }
        return 0;
    }
    private void RemoveNode(int x, int y)
    {
        Node node = _grid_T[x][y];
        _grid_T[x][y] = null;
        node.gameObject.SetActive(false);
        _nodeCache.Push(node);
    }
    private void RemoveNode2(int x, int y)
    {
        Node node = _grid_B[x][y];
        _grid_B[x][y] = null;
        node.gameObject.SetActive(false);
        _nodeCache.Push(node);
    }

    private Vector2 GridToWorld(int x, int y, int option)
    {
        switch (option)
        {
            case 1:
                return new Vector2(x - _xTrans, _yTrans_T - y);

            case 2: return new Vector2(x - _xTrans, _yTrans_B - y);

            case 3: return new Vector2(x - _xTrans, _yTrans_T - Height - y);

            default: return new Vector2(0, 0);
        }

    }
    private int CheckColumn(Node node, int option)
    {
        switch (option)
        {
            case 1:
                for (var i = 0; i < Width; i++)
                {

                    for (var j = 0; j < Height; j++)
                    {
                        if (node == _grid_T[i][j])
                        {
                            return (i - 2);
                        }
                    }
                }
                break;

            case 2:
                for (var i = 0; i < Width; i++)
                {

                    for (var j = 0; j < (MaxHeight - Height); j++)
                    {
                        if (node == _grid_B[i][j])
                        {
                            return (i - 2);
                        }
                    }
                }
                break;
        }
        return 0;
    }
    private bool CheckWidth(Node node)
    {
        
        if (CheckGrid(node) == 1)
        {
            if (GetConnected(node, true, 1) > _connectedCount)
            {
                for (var j = 0; j < (MaxHeight - Height); j++)
                {
                    GridNode_B[CheckColumn(node, 1) + 2][j].GetComponent<SpriteRenderer>().color = new Vector4(1.0f, 1.0f, 0.19f, 1.0f);

                }

                for (var i = 0; i < Width; i++)
                {

                    for (var j = 0; j < (Height); j++)
                    {
                        GridNode_T[i][j].GetComponent<SpriteRenderer>().color = new Vector4(1.0f, 1.0f, 1.0f, 1.0f);
                    }
                }
            }
        }
        if (CheckGrid(node) == 2)
        {
            if (GetConnected(node, true, 2) > _connectedCount)
            {
                for (var i = 0; i < Width; i++)
                {

                    for (var j = 0; j < (MaxHeight - Height); j++)
                    {
                        GridNode_B[i][j].GetComponent<SpriteRenderer>().color = new Vector4(1.0f, 1.0f, 1.0f, 1.0f);

                    }
                }


                for (var j = 0; j < (Height); j++)
                {
                    GridNode_T[CheckColumn(node, 2) + 2][j].GetComponent<SpriteRenderer>().color = new Vector4(1.0f, 1.0f, 0.19f, 1.0f);
                }
            }
        }
        if (CheckGrid(node) == 1)
        {

            if (CheckColumn(node, 1) == _column)
            {
                if (GetConnected(node, true, 1) > _connectedCount)
                {



                    if (CheckGrid(node) != Grid)
                    {
                        _combo += 1;
                    }
                    else
                    { 
                        Grid = CheckGrid(node);
                    }
                    switch (_column)
                    {
                        case -2:


                            target_LL = 5.0f;
                            _fadeTime_LL = _currTime + 0.5f;
                            _scoreTime_LL = _currTime + 2.25f;
                            Grid = CheckGrid(node);
                            _column = CheckColumn(node, 1);
                            break;
                        case -1:


                            target_L = 5.0f;
                            _fadeTime_L = _currTime + 0.5f;
                            _scoreTime_L = _currTime + 2.25f;
                            Grid = CheckGrid(node);
                            _column = CheckColumn(node, 1);
                            break;
                        case 0:


                            target_C = 5.0f;
                            _fadeTime_C = _currTime + 0.5f;
                            _scoreTime_C = _currTime + 2.25f;
                            Grid = CheckGrid(node);
                            _column = CheckColumn(node, 1);
                            break;
                        case 1:


                            target_R = 5.0f;
                            _fadeTime_R = _currTime + 0.5f;
                            _scoreTime_R = _currTime + 2.25f;
                            Grid = CheckGrid(node);
                            _column = CheckColumn(node, 1);
                            break;
                        case 2:


                            target_RR = 5.0f;
                            _fadeTime_RR = _currTime + 0.5f;
                            _scoreTime_RR = _currTime + 2.25f;
                            Grid = CheckGrid(node);
                            _column = CheckColumn(node, 1);
                            break;

                    }
                }
            }
            else
            {
                _column = CheckColumn(node, 1);
            }
        }
        if (CheckGrid(node) == 2)
        {
            if (CheckColumn(node, 2) == _column)
            {
                if (GetConnected(node, true, 2) > _connectedCount)
                {


                    if (CheckGrid(node) != Grid)
                    {
                        _combo += 1;
                    }
                    else
                    {
                        Grid = CheckGrid(node);
                    }
                    switch (_column)
                    {
                        case -2:


                            target_LL = 5.0f;
                            _fadeTime_LL = _currTime + 0.5f;
                            _scoreTime_LL = _currTime + 2.25f;
                            Grid = CheckGrid(node);
                            _column = CheckColumn(node, 2);
                            break;
                        case -1:


                            target_L = 5.0f;
                            _fadeTime_L = _currTime + 0.5f;
                            _scoreTime_L = _currTime + 2.25f;
                            Grid = CheckGrid(node);
                            _column = CheckColumn(node, 2);
                            break;
                        case 0:


                            target_C = 5.0f;
                            _fadeTime_C = _currTime + 0.5f;
                            _scoreTime_C = _currTime + 2.25f;
                            Grid = CheckGrid(node);
                            _column = CheckColumn(node, 2);
                            break;
                        case 1:

                            Grid = CheckGrid(node);
                            _column = CheckColumn(node, 2);

                            target_R = 5.0f;
                            _fadeTime_R = _currTime + 0.5f;
                            _scoreTime_R = _currTime + 2.25f;
                            break;
                        case 2:


                            target_RR = 5.0f;
                            _fadeTime_RR = _currTime + 0.5f;
                            _scoreTime_RR = _currTime + 2.25f;
                            Grid = CheckGrid(node);
                            _column = CheckColumn(node, 2);
                            break;

                    }
                }
            }
            else
            {
                _column = CheckColumn(node, 2);
            }
        }

        return false;
    }

    private IEnumerator GameOver()
    {
        GameOverUI.SetActive(true);
        yield return new WaitForSeconds(1);
        FindObjectOfType<LevelManager>().GoToMenu();
    }

    public void NextLevel()
    {
        FindObjectOfType<LevelManager>().NextLevel();
    }
}