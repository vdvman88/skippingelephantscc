﻿using UnityEngine;
using System.Collections.Generic;

public class Tweener : MonoBehaviour
{
    private class Tween
    {
        private const float _duration = 0.5f;
        private Transform _trans;
        private Vector2 _start;
        private Vector2 _end;
        private float _currTime;

        public Tween(Transform trans, Vector2 end)
        {
            _trans = trans;
            _start = trans.position;
            _end = end;
            _currTime = 0.0f;
        }

        public bool Update(float deltaTime)
        {
            _currTime += deltaTime;
            if (_currTime <= _duration)
            {
                Vector2 currentPos = Vector2.zero;
                currentPos.x = Mathf.Lerp(_start.x, _end.x, _currTime/_duration);
                currentPos.y = Mathf.Lerp(_start.y, _end.y, _currTime/_duration);
                _trans.position = currentPos;
                return true;
            }

            _trans.position = _end;
            return false;
        }
    }

    private List<Tween> _tweens;
    public bool IsTweening { get; private set; }

    public void Initialize(int capacity)
    {
        _tweens = new List<Tween>(capacity);
        IsTweening = false;
    }

    public void AddTween(Transform trans, Vector2 end)
    {
        _tweens.Add(new Tween(trans, end));
        IsTweening = true;
    }

    public void Update()
    {
        if (!IsTweening)
        {
            return;
        }
        if (_tweens.Count == 0)
        {
            IsTweening = false;
            return;
        }
        for (int i = _tweens.Count - 1; i >= 0; i--)
        {
            bool active = _tweens[i].Update(Time.deltaTime);
            if (!active)
            {
                _tweens.RemoveAt(i);
            }
        }
    }
}
