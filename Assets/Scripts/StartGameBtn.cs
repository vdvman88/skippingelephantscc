﻿using UnityEngine;
using System.Collections;

public class StartGameBtn : MonoBehaviour
{

    public LevelManager LevelManager;

    public void StartGame()
    {
        LevelManager.NextLevel();
    }
}
