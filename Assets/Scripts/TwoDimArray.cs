﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets.Scripts
{
    [Serializable]
    public class TwoDimArray
    {
        [Serializable]
        public struct RowData
        {
            public bool[] Row;
        }

        public RowData[] Rows = new RowData[8];

        public bool this[int x, int y]
        {
            get { return Rows[y].Row[x]; }
        }
    }
}
