﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets.Scripts
{
    class BurstNode : SpecialNode
    {

        public override int OnRemoved(NodeManager nodeManager, int scoreChange)
        {
            return scoreChange;
        }

        public override bool Matches(Node other)
        {
            return true;    //propagates matching to all connected nodes
        }
    }

}
