﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class LevelSelect : MonoBehaviour
{

    public GameObject Levels;
    public GameObject LevelButton;

    void Start()
    {
        var manager = FindObjectOfType<LevelManager>();
        foreach (string level in manager.Levels)
        {
            GameObject go = Instantiate(LevelButton);
            go.GetComponent<LevelBtn>().LevelName = level;
            go.transform.SetParent(Levels.transform, false);
        }
    }

}
