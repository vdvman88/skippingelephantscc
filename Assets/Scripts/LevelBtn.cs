﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class LevelBtn : MonoBehaviour
{
    public string LevelName;

    void Start()
    {
        GetComponentInChildren<Text>().text = LevelName;
    }
    public void GoToLevel()
    {
        FindObjectOfType<LevelManager>().GoToLevel(LevelName);
    }
}
