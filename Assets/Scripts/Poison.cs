﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets.Scripts
{
    class Poison : SpecialNode
    {
        public override int OnRemoved(NodeManager nodeManager, int scoreChange)
        {
            nodeManager.NumPoisonBeingProcessed += 1;
            return scoreChange;
        }
    }
}
