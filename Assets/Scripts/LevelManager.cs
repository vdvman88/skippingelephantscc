﻿using System;
using UnityEngine;
using System.Collections;
using System.Linq;
using UnityEngine.SceneManagement;

public class LevelManager : MonoBehaviour
{
    public string[] Levels;
    public string Main;
    public string LevelSelect;

    [HideInInspector] public bool LevelSelected = false;

    private int _currLevel = -1;

    void Start()
    {
        DontDestroyOnLoad(this);
    }

    public void NextLevel()
    {
        if (LevelSelected)
        {
            GoToLevelSelect();
        }
        else
        {
            _currLevel++;
            if (_currLevel < Levels.Length)
            {
                SceneManager.LoadScene(Levels[_currLevel]);
            }
            else
            {
                GoToMenu();
            }
        }
    }

    public void GoToMenu()
    {
        _currLevel = -1;
        LevelSelected = false;
        SceneManager.LoadScene(Main);
    }

    public void GoToLevelSelect()
    {
        _currLevel = -1;
        LevelSelected = true;
        SceneManager.LoadScene(LevelSelect);
    }

    public void GoToLevel(string lvlName)
    {
        int lvl = Array.IndexOf(Levels, lvlName);
        if (lvl < 0)
        {
            return;
        }
        _currLevel = lvl;
        SceneManager.LoadScene(lvlName);
    }
}
